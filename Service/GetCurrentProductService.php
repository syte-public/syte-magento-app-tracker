<?php
declare(strict_types=1);

namespace Syte\Tracker\Service;

use Magento\Catalog\Api\Data\ProductInterface;
use Magento\Catalog\Api\ProductRepositoryInterface;
use Magento\Catalog\Model\SessionFactory as CatalogSession;
use Magento\Framework\Exception\NoSuchEntityException;

/**
 * Current Product Data Provider
 *
 * @SuppressWarnings(PHPMD.CookieAndSessionMisuse)
 */
class GetCurrentProductService
{
    /**
     * @var ProductInterface
     */
    private $currentProduct;

    /**
     * @var int|null
     */
    private $productId;

    /**
     * @var CatalogSession
     */
    private $catalogSession;

    /**
     * @var ProductRepositoryInterface
     */
    private $productRepository;

    /**
     * GetCurrentProductService constructor
     *
     * @param CatalogSession $catalogSession
     * @param ProductRepositoryInterface $productRepository
     */
    public function __construct(
        CatalogSession $catalogSession,
        ProductRepositoryInterface $productRepository
    ) {
        $this->catalogSession = $catalogSession;
        $this->productRepository = $productRepository;
    }

    /**
     * Get Product ID
     *
     * @return int|null
     */
    public function getProductId(): ?int
    {
        if (!$this->productId) {
            $catalogSession = $this->catalogSession->create();
            $productId = $catalogSession->getData('last_viewed_product_id');
            $this->productId = $productId ? (int)$productId : null;
        }

        return $this->productId;
    }

    /**
     * Get product object
     *
     * @return ProductInterface|null
     */
    public function getProduct(): ?ProductInterface
    {
        if (!$this->currentProduct) {
            $productId = $this->getProductId();
            if (!$productId) {
                return null;
            }

            try {
                $this->currentProduct =  $this->productRepository->getById($this->getProductId());
            } catch (NoSuchEntityException $e) {
                return null;
            }
        }

        return $this->currentProduct;
    }
}
